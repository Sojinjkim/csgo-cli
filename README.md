CSGO-CLI

Follow csgo games on your console. Perfect for procrastinating while at work when you can't pull up those twitch/youtube streams.

`csgo-cli` has one command. (working on getting team and player stats stuff!)


### Csgo

It will pull up COMPLETED, LIVE and UPCOMING matches.

1. Selecting a COMPLETED match will pull up an overview of the maps played as well as the box scores for each map.

2. Selecting a LIVE match will bring up a live updating scorboard that includes a boxscore and play-by-play.

3. Selecting an UPCOMING match will show a preview of the two teams. This includes the HLTV ranks for the teams, HLTV Ratings for the players, match description and betting odds if they are available.


## Development

It's simple to run `csgo-cli` on your local computer.  
The following is step-by-step instruction.

```
$ git clone https://gitlab.com/Sojinjkim/csgo-cli.git
$ cd csgo-cli
$ yarn
$ NODE_ENV=development node bin/cli.js csgo
```

If you have trouble running after selecting a match, update the hltv npm package.

```
$ npm i hltv
```


## License

MIT ©
