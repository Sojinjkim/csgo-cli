import { error } from './log';

const catchAPIError = (err, apiName) => {
  console.log('');
  console.log('');
  error(err);
  console.log('');
  error(`Oops, ${apiName} goes wrong.`);
  error(
    'Please run csgo-cli again.\nIf it still does not work, you can start a ticket at https://gitlab.com/Sojinjkim/csgo-cli/issues. (1v1 me in TFT, Kalashdow)'
  );

  process.exit(1);
};

export default catchAPIError;
