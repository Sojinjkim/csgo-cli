import chalk from 'chalk';
import { dayNames, monthNames } from './mapConstants';

export const error = msg => {
  console.log(chalk`{red.bold ${msg}}`);
};

export const bold = msg => chalk`{white.bold ${msg}}`;

export const neonRed = msg => chalk`{bold.hex('#f00b47') ${msg}}`;

export const neonGreen = msg => chalk`{hex('#66ff66') ${msg}}`;

export const tOrange = msg => chalk`{hex('#ffa02b') ${msg}}`;

export const ctBlue = msg => chalk`{hex('#5797ff') ${msg}}`;

export const colorTeamName = (color, name) =>
  chalk`{bold.white.bgHex('${color}') ${name}}`;

// RETURNS ALL OF THE PLAYS FOR THE LIVE PLAY BY PLAY
export const updatedPlayByPlayData = (oldPlays, newPlays) => {
  let allPlays;
  if (oldPlays.log) {
    oldPlays.log.unshift(newPlays.log[0]);
    allPlays = oldPlays;
  } else {
    allPlays = newPlays;
  }

  return allPlays;
};

// Returns the date in text Ex. 'Sun Jul 14 2019 11:00:00'
export const localTime = dateObject => {
  const dayNum = dateObject.getDate();
  const dayName = dayNames[dateObject.getDay()];
  const monthName = monthNames[dateObject.getMonth()];
  const year = dateObject.getUTCFullYear();
  const time = dateObject.toLocaleTimeString();

  return `${dayName} ${monthName} ${dayNum} ${year} ${time}`;
};
