// exporting a map for abbreviations for HLTV map names
export const mapNames = {
  d2: 'Dust 2',
  ovp: 'Overpass',
  trn: 'Train',
  mrg: 'Mirage',
  vertigo: 'Vertigo',
  inf: 'Inferno',
  nuke: 'Nuke',
};

// Gives the abreviation for a month given the getMonth index
export const monthNames = {
  0: 'Jan',
  1: 'Feb',
  2: 'Mar',
  3: 'Apr',
  4: 'May',
  5: 'Jun',
  6: 'Jul',
  7: 'Aug',
  8: 'Sep',
  9: 'Oct',
  10: 'Nov',
  11: 'Dec',
};

// Gives the abreviation for the day given the getDay index
export const dayNames = {
  0: 'Sun',
  1: 'Mon',
  2: 'Tue',
  3: 'Wed',
  4: 'Thu',
  5: 'Fri',
  6: 'Sat',
};
