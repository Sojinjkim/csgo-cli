import { left } from 'wide-align';

import { bold, neonGreen, neonRed, tOrange, ctBlue } from './log';

// Get the color for the team name (orange or blue)
export const teamNameColor = (teamName, teamId, scoreboardUpdate) => {
  if (teamId === scoreboardUpdate.tTeamId) {
    return tOrange(`${teamName}`);
  }
  return ctBlue(`${teamName}`);
};

// Get the list of players for the teams
export const selectTeamPlayers = (teamId, scoreboardUpdate) => {
  if (teamId === scoreboardUpdate.tTeamId) {
    return scoreboardUpdate.TERRORIST;
  }
  return scoreboardUpdate.CT;
};

// Get the round score of the current map
export const getTeamRoundScore = (teamId, scoreboardUpdate) => {
  if (teamId === scoreboardUpdate.tTeamId) {
    return scoreboardUpdate.tTeamScore.toString();
  }
  return scoreboardUpdate.ctTeamScore.toString();
};

// Determing the winner of the maps in mapResult and highlight it neon green
export const mapWinnerColor = (teamScore, oppScore) => {
  if (teamScore > oppScore) {
    return neonGreen(teamScore.toString());
  }
  return teamScore.toString();
};

// Get the map names in the scoreboard
export const getMapScoreTableHeader = selectedMatch => {
  const mapScoreTableHeader = [''];
  for (let index = 0; index < selectedMatch.maps.length; index += 1) {
    mapScoreTableHeader.push(bold(selectedMatch.maps[index].name));
  }

  mapScoreTableHeader.push(bold('Map Total'));
  return mapScoreTableHeader;
};

// Get the teams scores from mapResults
export const getTeamMapsRoundScore = (
  team,
  mapResults,
  selectedMatch,
  currTeamScore
) => {
  const teamMapScore = [bold(`${team.name}`)];
  let teamScore = 0;
  for (let index = 0; index < mapResults.length; index += 1) {
    if (mapResults[index]) {
      const isTeamOne = team.id === mapResults[index].team1.id;
      if (isTeamOne) {
        teamMapScore.push(
          bold(
            mapWinnerColor(
              mapResults[index].team1.score,
              mapResults[index].team2.score
            )
          )
        );
        if (mapResults[index].team1.score > mapResults[index].team2.score) {
          teamScore += 1;
        }
      } else {
        teamMapScore.push(
          bold(
            mapWinnerColor(
              mapResults[index].team2.score,
              mapResults[index].team1.score
            )
          )
        );
        if (mapResults[index].team2.score > mapResults[index].team1.score) {
          teamScore += 1;
        }
      }
    }
  }

  teamMapScore.push(`${currTeamScore}`);
  for (
    let index = mapResults.length;
    index < selectedMatch.maps.length - 1;
    index += 1
  ) {
    teamMapScore.push('  ');
  }

  teamMapScore.push(teamScore.toString());

  return teamMapScore;
};

export const getTeamBoxscore = (team, playersData) => {
  const teamBoxscoreRows = [];
  teamBoxscoreRows.push([
    team.name,
    bold('Kills'),
    bold('Assists'),
    bold('Deaths'),
    bold('ADR'),
    bold('$$$'),
    bold('ARMOR'),
    bold('WEAPON'),
    bold('KIT'),
    bold('HP'),
  ]);

  playersData.forEach(player => {
    teamBoxscoreRows.push([
      bold(left(player.nick, 14)),
      left(`${player.score}`, 5),
      left(`${player.assists}`, 5),
      left(`${player.deaths}`, 5),
      left(`${player.damagePrRound.toFixed(2)}`, 7),
      left(`$${player.money}`, 7),
      left(`${player.kevlar ? 'YES' : 'NO'}`, 5),
      left(`${player.primaryWeapon ? player.primaryWeapon : '---'}`, 7),
      left(`${player.hasDefusekit ? 'YES' : '---'}`, 5),
      player.alive
        ? left(neonGreen(`${player.hp}`, 5))
        : left(neonRed('DEAD', 5)),
    ]);
  });

  return teamBoxscoreRows;
};

// Adds the orange or blue for the side name
const playByPlayColor = (side, text) => {
  if (side === 'TERRORIST') {
    return tOrange(`${text}`);
  }
  return ctBlue(`${text}`);
};

// Returns the end of round text
const roundEndText = (winner, tScore, ctScore, tTeamName, ctTeamName) =>
  winner === 'TERRORIST'
    ? `${tOrange(tScore)} - ${ctBlue(ctScore)} ${playByPlayColor(
        winner,
        `${tTeamName}`
      )} won the round`
    : `${tOrange(tScore)} - ${ctBlue(ctScore)} ${playByPlayColor(
        winner,
        `${ctTeamName}`
      )} won the round`;

// Returns the text for the play by play
export const csgoPlayByPlay = (events, tTeamName, ctTeamName) => {
  const playByPlayRows = [];
  let newEvent;
  for (let index = 0; index < events.length; index += 1) {
    if (events[index].BombDefused) {
      newEvent = events[index].BombDefused;
      playByPlayRows.push(
        `${playByPlayColor('CT', newEvent.playerNick)} has defused the bomb`
      );
    }

    if (events[index].BombPlanted) {
      newEvent = events[index].BombPlanted;
      playByPlayRows.push(
        `${playByPlayColor('TERRORIST', newEvent.tPlayers)} - ${playByPlayColor(
          'CT',
          newEvent.ctPlayers
        )} players remaining post plant`
      );
      playByPlayRows.join('\n');
      playByPlayRows.push(
        `${playByPlayColor(
          'TERRORIST',
          newEvent.playerNick
        )} has planted the bomb`
      );
    }

    if (events[index].Kill) {
      newEvent = events[index].Kill;
      playByPlayRows.push(
        `${playByPlayColor(
          newEvent.killerSide,
          newEvent.killerNick
        )} killed ${playByPlayColor(
          newEvent.victimSide,
          newEvent.victimNick
        )} with ${newEvent.weapon}`
      );
    }

    if (events[index].MatchStarted) {
      newEvent = events[index].MatchStarted;
      playByPlayRows.push(`The match has started - ${newEvent.map}`);
      playByPlayRows.push('\n');
      playByPlayRows.push('Done with warmups');
    }

    if (events[index].PlayerJoin) {
      newEvent = events[index].PlayerJoin;
      playByPlayRows.push(`${newEvent.playerNick} has joined`);
    }

    if (events[index].PlayerQuit) {
      newEvent = events[index].PlayerQuit;
      playByPlayRows.push(
        `${playByPlayColor(newEvent.playerSide, newEvent.playerNick)} has left`
      );
    }

    if (events[index].Restart) {
      playByPlayRows.push('The map is being restarted');
    }

    if (events[index].RoundEnd) {
      newEvent = events[index].RoundEnd;
      if (newEvent.terroristScore + newEvent.counterTerroristScore === 15) {
        playByPlayRows.push('END OF FIRST HALF');
        playByPlayRows.push('SWITCHING SIDES');
        playByPlayRows.push('\n');
      }
      playByPlayRows.push(
        roundEndText(
          newEvent.winner,
          newEvent.terroristScore,
          newEvent.counterTerroristScore,
          tTeamName,
          ctTeamName
        )
      );
    }

    if (events[index].RoundStart) {
      playByPlayRows.push('Round Start');
      playByPlayRows.push('\n');
    }

    if (events[index].Suicide) {
      newEvent = events[index].Suicide;
      playByPlayRows.push(
        `${playByPlayColor(
          newEvent.side,
          newEvent.playerNick
        )} has brought shame to his family and committed sudoku`
      );
    }
  }

  return playByPlayRows.join('\n');
};
