import path from 'path';
import blessed from 'blessed';
import { right } from 'wide-align';

const getCsgoLiveComponents = (teamOne, teamTwo) => {
  const screen = blessed.screen({
    smartCSR: true,
    fullUnicode: true,
    title: 'CSGO-CLI',
  });

  const baseBox = blessed.box({
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    padding: 0,
    style: {
      fg: 'black',
      bg: 'black',
      border: {
        fg: '#f0f0f0',
        bg: 'black',
      },
    },
  });

  const mapScoreTable = blessed.table({
    top: 5,
    left: 'center',
    width: '33%',
    height: 8,
    tags: true,
    border: {
      type: 'line',
    },
    style: {
      header: {
        fg: 'white',
      },
      cell: {
        fg: 'white',
      },
    },
  });

  const teamOneNameText = blessed.text({
    parent: screen,
    top: 7,
    left: `33%-${teamOne.name.length + 24}`,
    width: 25,
    align: 'left',
    style: {
      fg: 'white',
    },
  });

  const teamOneRankText = blessed.text({
    top: 8,
    left: '33%-39',
    width: 15,
    align: 'right',
    content: right(teamOne.rank ? `Rank #${teamOne.rank}` : 'Unranked', 15),
    style: {
      fg: '#fbfbfb',
    },
  });

  const teamOneScoreText = blessed.bigtext({
    font: path.join(__dirname, './fonts/ter-u12n.json'),
    fontBold: path.join(__dirname, './fonts/ter-u12b.json'),
    top: 2,
    left: '33%-20',
    width: 15,
    align: 'right',
    vlign: 'center',
    style: {
      fg: 'white',
    },
  });

  const teamTwoNameText = blessed.text({
    top: 7,
    left: '66%+28',
    width: 25,
    align: 'left',
    tags: true,
    style: {
      fg: 'white',
    },
  });

  const teamTwoRankText = blessed.text({
    top: 8,
    left: '66%+28',
    width: 15,
    align: 'left',
    content: teamTwo.rank ? `Rank #${teamTwo.rank}` : 'Unranked',
    style: {
      fg: '#fbfbfb',
    },
  });

  const teamTwoScoreText = blessed.bigtext({
    // font: path.join(__dirname, './fonts/ter-u12n.json'),
    // fontBold: path.join(__dirname, './fonts/ter-u12b.json'),
    top: 2,
    left: '66%+6',
    width: 15,
    align: 'left',
    style: {
      fg: 'white',
    },
  });

  const competitionName = blessed.text({
    top: 0,
    left: 'center',
    align: 'center',
    style: {
      fg: 'white',
    },
  });

  const matchDescription = blessed.text({
    top: 1,
    left: 'center',
    align: 'center',
    style: {
      fg: 'white',
    },
  });

  const matchFormat = blessed.text({
    top: 2,
    left: 'center',
    align: 'center',
    style: {
      fg: 'white',
    },
  });

  const mapNameText = blessed.text({
    top: 13,
    left: 'center',
    align: 'center',
    style: {
      fg: 'white',
    },
  });

  const streamName = blessed.text({
    top: 4,
    left: 'center',
    align: 'center',
    style: {
      fg: 'white',
    },
  });

  const playByPlayBox = blessed.box({
    parent: screen,
    top: 15,
    left: 3,
    width: '40%-3',
    height: '100%-15',
    padding: {
      top: 0,
      right: 0,
      left: 2,
      bottom: 0,
    },
    align: 'left',
    keys: true,
    mouse: false,
    scrollable: true,
    focused: true,
    label: ' Play By Play ',
    border: {
      type: 'line',
    },
    scrollbar: {
      ch: ' ',
      track: {
        bg: '#0253a4',
      },
      style: {
        inverse: true,
      },
    },
  });

  const boxscoreTable = blessed.table({
    parent: screen,
    top: 15,
    left: '40%',
    width: '60%-3',
    height: '100%-15',
    tags: true,
    pad: 0,
    label: ' Box Score ',
    border: {
      type: 'line',
    },
  });

  screen.append(baseBox);
  screen.append(matchDescription);
  screen.append(matchFormat);
  screen.append(competitionName);
  screen.append(mapNameText);
  screen.append(streamName);
  screen.append(teamOneNameText);
  screen.append(teamOneRankText);
  screen.append(teamOneScoreText);
  screen.append(teamTwoNameText);
  screen.append(teamTwoRankText);
  screen.append(teamTwoScoreText);
  screen.append(mapScoreTable);
  screen.append(playByPlayBox);
  screen.append(boxscoreTable);
  screen.key(['escape', 'q', 'C-c'], () => process.exit(1));

  return {
    screen,
    mapScoreTable,
    matchDescription,
    matchFormat,
    competitionName,
    mapNameText,
    streamName,
    teamOneNameText,
    teamTwoNameText,
    teamOneScoreText,
    teamTwoScoreText,
    playByPlayBox,
    boxscoreTable,
  };
};

export default getCsgoLiveComponents;
