/* eslint-disable no-await-in-loop, no-constant-condition */

import R from 'ramda';
import parse from 'date-fns/parse';
import addDays from 'date-fns/add_days';
import subDays from 'date-fns/sub_days';
import format from 'date-fns/format';
import isValid from 'date-fns/is_valid';
import emoji from 'node-emoji';
import ora from 'ora';

import { HLTV } from 'hltv';
import chooseCsgoGameFromSchedule, { getCsgoTeamInfo } from './csgoSchedule';
import csgoPreview from './csgoPreview';
import csgoScoreboard from './csgoScoreboard';
import csgoBoxScore from './csgoBoxScore';

import { error, bold, updatedPlayByPlayData } from '../../utils/log';
import { cfontsDate } from '../../utils/cfonts';
import getCsgoLiveComponents from '../../utils/csgoLiveComponents';
import catchAPIError from '../../utils/catchAPIError';
import csgoLive from './csgoLive';

const getGameWithOptionalFilter = async csgoGames =>
  chooseCsgoGameFromSchedule(csgoGames);

const csgo = async option => {
  let _date;

  // Display the date
  // Allow for using the option in the future. Can use for looking up player or team stats
  if (option.date) {
    if (
      R.compose(
        isValid,
        parse
      )(option.date)
    ) {
      _date = format(option.date, 'YYYY-MM-DD');
    } else {
      error('Date is invalid');
      process.exit(1);
    }
  } else if (option.today) {
    _date = Date.now();
  } else if (option.tomorrow) {
    _date = addDays(Date.now(), 1);
  } else if (option.yesterday) {
    _date = subDays(Date.now(), 1);
  } else {
    error(`Can't find any option ${emoji.get('confused')}`);
    process.exit(1);
  }

  R.compose(cfontsDate)(_date);

  const csgoGames = [];

  // GET ALL UPCOMING CSGO GAMES AND FILTER OUT THE 0 STAR GAMES
  // I ONLY CARE ABOUT THE TOP TEAMS

  try {
    const moreCsgoGames = await HLTV.getMatches();
    const finishedGames = await HLTV.getResults({ pages: 1 });

    for (let index = finishedGames.length - 1; index >= 0; index -= 1) {
      if (
        finishedGames[index].team1 !== undefined &&
        finishedGames[index].team2 !== undefined &&
        finishedGames[index].stars >= 1
      ) {
        csgoGames.push(finishedGames[index]);
      }
    }

    for (let index = 0; index < moreCsgoGames.length; index += 1) {
      if (
        moreCsgoGames[index].team1 !== undefined &&
        moreCsgoGames[index].team2 !== undefined &&
        moreCsgoGames[index].stars >= 1
      ) {
        csgoGames.push(moreCsgoGames[index]);
      }
    }
  } catch (err) {
    catchAPIError(err, 'HLTV.getMatches() or HLTV.getResults()');
  }

  // Return each team and the match that was seleted from the schedule

  const {
    game: { teamOne, teamTwo, csgoGame },
  } = await getGameWithOptionalFilter(csgoGames);

  // Load the selected match

  let spinner = ora('Loading Match').start();

  let selectedMatch;
  try {
    selectedMatch = await HLTV.getMatch({ id: csgoGame.id });
  } catch (err) {
    catchAPIError(err, 'HLTV.getMatch()');
  }

  spinner.stop();

  // Get the full team object from HLTV

  spinner = ora('Loading Full Teams').start();

  let teamOneObject;
  let teamTwoObject;
  try {
    teamOneObject = await HLTV.getTeam({ id: teamOne.id });
    teamTwoObject = await HLTV.getTeam({ id: teamTwo.id });
  } catch (err) {
    catchAPIError(err, 'HLTV.getTeam()');
  }

  spinner.stop();

  // Switch case to correctly display the output depending on the status of the match

  switch (selectedMatch.status) {
    case 'Scheduled': {
      // FETCH THE TEAMS FOR PREVIEW
      spinner = ora('Loading Teams For Match Preview').start();

      const teamOnePreview = await getCsgoTeamInfo(teamOne);
      const teamTwoPreview = await getCsgoTeamInfo(teamTwo);

      spinner.stop();

      spinner = ora('Loading Team Players').start();

      const teamOnePlayers = [];
      const teamTwoPlayers = [];

      // Fetch the players for the teams for the preview

      try {
        for (let index = 0; index < teamOnePreview.players.length; index += 1) {
          let playerFound = null;
          if (!teamOnePreview.players[index].id) {
            playerFound = {
              ign: teamOnePreview.players[index].name,
              statistics: NaN,
            };
          } else {
            playerFound = await HLTV.getPlayer({
              id: teamOnePreview.players[index].id,
            });
          }
          teamOnePlayers.push(playerFound);
        }
        for (let index = 0; index < teamTwoPreview.players.length; index += 1) {
          let playerFound = null;
          if (!teamTwoPreview.players[index].id) {
            playerFound = {
              ign: teamTwoPreview.players[index].name,
              statistics: NaN,
            };
          } else {
            playerFound = await HLTV.getPlayer({
              id: teamTwoPreview.players[index].id,
            });
          }
          teamTwoPlayers.push(playerFound);
        }
      } catch (err) {
        catchAPIError(err, 'HLTV.getPlayer()');
      }

      spinner.stop();

      const date = new Date(csgoGame.date);

      csgoPreview(
        selectedMatch,
        teamOnePreview,
        teamTwoPreview,
        teamOnePlayers,
        teamTwoPlayers,
        date
      );
      break;
    }

    case 'LIVE':
    case '2': {
      // Create the screen and format the scoreboard and play by play for the live component

      const {
        screen,
        mapScoreTable,
        matchDescription,
        matchFormat,
        competitionName,
        mapNameText,
        streamName,
        teamOneNameText,
        teamTwoNameText,
        teamOneScoreText,
        teamTwoScoreText,
        playByPlayBox,
        boxscoreTable,
      } = getCsgoLiveComponents(teamOneObject, teamTwoObject, selectedMatch);

      // Load in the current match results to input the score on the scoreboard

      spinner = ora('Loading Current Match Results').start();

      const mapResults = [];
      try {
        let currentMapStats;
        for (let index = 0; index < selectedMatch.maps.length; index += 1) {
          if (selectedMatch.maps[index].statsId !== undefined) {
            currentMapStats = await HLTV.getMatchMapStats({
              id: selectedMatch.maps[index].statsId,
            });
            mapResults.push(currentMapStats);
          }
        }
      } catch (err) {
        catchAPIError(err, 'HLTV.getMatchStats');
      }

      spinner.stop();

      let playByPlayData = [];
      let newScoreboardData = null;
      competitionName.setContent(bold(`${selectedMatch.event.name}`));
      matchDescription.setContent(`${selectedMatch.additionalInfo}`);
      matchFormat.setContent(`${selectedMatch.format}`);
      streamName.setContent(
        `${
          selectedMatch.streams.length > 0
            ? `${selectedMatch.streams[0].link}  ${emoji.get('tv')}`
            : 'There is no stream available'
        }`
      );

      // Connect to the scorebot and update the scoreboard and play by play features live

      try {
        await HLTV.connectToScorebot({
          id: selectedMatch.id,
          onLogUpdate: logData => {
            playByPlayData = updatedPlayByPlayData(playByPlayData, logData);
            if (playByPlayData.log && newScoreboardData) {
              csgoLive(
                teamOneObject,
                teamTwoObject,
                selectedMatch,
                mapResults,
                newScoreboardData,
                playByPlayData,
                {
                  screen,
                  mapScoreTable,
                  mapNameText,
                  teamOneNameText,
                  teamTwoNameText,
                  teamOneScoreText,
                  teamTwoScoreText,
                  playByPlayBox,
                  boxscoreTable,
                }
              );
            }
          },
          onScoreboardUpdate: scoreboardData => {
            newScoreboardData = scoreboardData;
            if (playByPlayData.log && newScoreboardData) {
              csgoLive(
                teamOneObject,
                teamTwoObject,
                selectedMatch,
                mapResults,
                newScoreboardData,
                playByPlayData,
                {
                  screen,
                  mapScoreTable,
                  mapNameText,
                  teamOneNameText,
                  teamTwoNameText,
                  teamOneScoreText,
                  teamTwoScoreText,
                  playByPlayBox,
                  boxscoreTable,
                }
              );
            }
          },
          onConnect() {
            console.log('WE HAVE CONNECTED BOIS');
          },
        });
      } catch (err) {
        catchAPIError(err, 'HLTV.connectToScorebot()');
      }

      break;
    }

    case 'Match over':
    default: {
      // Get the completed match results

      spinner = ora('Loading Match Results').start();

      const mapResults = [];
      try {
        let currentMapStats;
        for (let index = 0; index < selectedMatch.maps.length; index += 1) {
          if (selectedMatch.maps[index].statsId !== undefined) {
            currentMapStats = await HLTV.getMatchMapStats({
              id: selectedMatch.maps[index].statsId,
            });
            mapResults.push(currentMapStats);
          }
        }
      } catch (err) {
        catchAPIError(err, 'HLTV.getMatchStats');
      }

      spinner.stop();

      console.log('');
      csgoScoreboard(selectedMatch, mapResults);
      console.log('');
      csgoBoxScore(selectedMatch.maps, mapResults);
      console.log('');
    }
  }
};

export default csgo;
