import inquirer from 'inquirer';
import emoji from 'node-emoji';
import { limit } from 'stringz';
import { center, left, right } from 'wide-align';
import pMap from 'p-map';
import ora from 'ora';

import { HLTV } from 'hltv';
import CsgoTeam from '../CsgoTeam';

import { bold, localTime } from '../../utils/log';
import catchAPIError from '../../utils/catchAPIError';

const MAX_WIDTH = 145;
const TEAMNAME_WIDTH = 20;
const STATUS_WIDTH = 40;
const EVENT_NAME_WIDTH = 40;
const SCORE_WIDTH = 11;

const padHomeTeamName = name => bold(right(name, TEAMNAME_WIDTH));
const padVisitorTeamName = name => bold(left(name, TEAMNAME_WIDTH));
const padGameStatus = status => center(status, STATUS_WIDTH);
const padEventTitle = eventTitle => center(eventTitle, EVENT_NAME_WIDTH);
const padGameScore = score => center(score, SCORE_WIDTH);

const createGameChoice = csgoGame => {
  const homeTeamName = padHomeTeamName(csgoGame.team1.name);
  const visitorTeamName = padVisitorTeamName(csgoGame.team2.name);
  const match = `${homeTeamName}${center(
    emoji.get('vs'),
    8
  )}${visitorTeamName}`;
  let score = padGameScore('0 - 0');
  if (csgoGame.result) {
    score = padGameScore(csgoGame.result);
  }

  let date = 'LIVE';
  if (!csgoGame.live || csgoGame.result) {
    date = new Date(csgoGame.date);
  }

  return `│⌘${match}│${score}│${padGameStatus(
    `${date === 'LIVE' ? bold(date) : bold(localTime(date))}`
  )}│${padEventTitle(`${bold(csgoGame.event.name)}`)}│`;
};

const getCsgoTeamInfo = async team => {
  try {
    const foundTeam = await HLTV.getTeam({
      id: team.id,
    });

    return new CsgoTeam({
      teamId: foundTeam.id,
      teamCountry: foundTeam.location,
      teamName: foundTeam.name,
      rank: foundTeam.rank,
      players: foundTeam.players,
    });
  } catch (err) {
    catchAPIError(err, 'HLTV.getTeam()');
  }
};

// CHOOSE WHICH GAME YOU WANT TO WATCH ON THE SCEDULE
const chooseCsgoGameFromSchedule = async csgoGames => {
  const spinner = ora(
    `Loading Game Schedule...(0/${csgoGames.length})`
  ).start();

  const eventTitle = 'Event Title';

  const header = `│ ${padHomeTeamName('Team 1')}${center(
    emoji.get('gun'),
    8
  )}${padVisitorTeamName('Team 2')}│${center('Score', 11)}│${padGameStatus(
    'Status'
  )}│${padEventTitle(eventTitle)}|`;

  const tableWidth = MAX_WIDTH;
  const questions = [
    {
      name: 'game',
      message: 'Which game do you want to watch?',
      type: 'list',
      pageSize: 30,
      choices: [
        new inquirer.Separator(`${limit('', tableWidth, '─')}`),
        new inquirer.Separator(header),
        new inquirer.Separator(`${limit('', tableWidth, '─')}`),
      ],
    },
  ];

  const last = csgoGames.length - 1;

  await pMap(
    csgoGames,
    async (csgoGame, index) => {
      const { team1: teamOne, team2: teamTwo } = csgoGame;

      spinner.text = `Loading Game Schedule...(${index + 1}/${
        csgoGames.length
      })`;

      questions[0].choices.push({
        name: createGameChoice(csgoGame),
        value: { csgoGame, teamOne, teamTwo },
      });

      if (index !== last) {
        questions[0].choices.push(
          new inquirer.Separator(`${limit('', tableWidth, '─')}`)
        );
      } else {
        questions[0].choices.push(
          new inquirer.Separator(`${limit('', tableWidth, '─')}`)
        );
      }
    },
    { concurrency: 1 }
  );

  spinner.stop();

  const answer = await inquirer.prompt(questions);
  return answer;
};

export default chooseCsgoGameFromSchedule;
export { getCsgoTeamInfo };
