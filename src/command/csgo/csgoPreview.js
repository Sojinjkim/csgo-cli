import { center } from 'wide-align';

import emoji from 'node-emoji';
import { bold, localTime } from '../../utils/log';
import { basicTable } from '../../utils/table';

const alignCenter = columns =>
  columns.map(column => {
    if (typeof column === 'string') {
      return { content: column, vAlign: 'center', hAlign: 'center' };
    }

    return { ...column, vAlign: 'center', hAlign: 'center' };
  });

const createTeamStatsColumns = (
  teamName,
  whichTeam,
  rank,
  [playerOne, playerTwo, playerThree, playerFour, playerFive],
  selectedMatch
) => [
  teamName,
  `${rank}`,
  `${playerOne.ign} ${
    playerOne.statistics.rating ? playerOne.statistics.rating : 'N/A'
  }`,
  `${playerTwo.ign} ${
    playerTwo.statistics.rating ? playerTwo.statistics.rating : 'N/A'
  }`,
  `${playerThree.ign} ${
    playerThree.statistics.rating ? playerThree.statistics.rating : 'N/A'
  }`,
  `${playerFour.ign} ${
    playerFour.statistics.rating ? playerFour.statistics.rating : 'N/A'
  }`,
  `${playerFive.ign} ${
    playerFive.statistics.rating ? playerFive.statistics.rating : 'N/A'
  }`,
  whichTeam === 1
    ? `${selectedMatch.odds[0].team1}`
    : `${selectedMatch.odds[0].team2}`,
  whichTeam === 1
    ? `${selectedMatch.odds[1].team1}`
    : `${selectedMatch.odds[1].team2}`,
  whichTeam === 1
    ? `${selectedMatch.oddsCommunity.team1}`
    : `${selectedMatch.oddsCommunity.team2}`,
];

const csgoPreview = (
  selectedMatch,
  teamOne,
  teamTwo,
  teamOnePlayers,
  teamTwoPlayers,
  date
) => {
  const gamePreviewTable = basicTable();
  const columnMaxWidth = Math.max(teamOne.name.length, teamTwo.name.length);
  const teamOneNumber = 1;
  const teamTwoNumber = 2;

  gamePreviewTable.push(
    // DATE
    alignCenter([
      {
        colSpan: 16,
        content: bold(localTime(date)),
      },
    ]),

    // WHERE TO STREAM IT
    alignCenter([
      {
        colSpan: 16,
        content: selectedMatch.streams[0]
          ? bold(`${selectedMatch.streams[0].link} ${emoji.get('tv')}`)
          : bold('No streams available yet T.T'),
      },
    ]),

    // EVENT INFO
    alignCenter([
      {
        colSpan: 16,
        content: bold(`${selectedMatch.event.name}`),
      },
    ]),

    // ADDITIONAL MATCH INFO
    alignCenter([
      {
        colSpan: 16,
        content: bold(`${selectedMatch.additionalInfo}`),
      },
    ]),

    // FORMAT OF THE MATCH
    alignCenter([
      {
        colSpan: 16,
        content: bold(`${selectedMatch.format}`),
      },
    ]),

    // TEAM ONE INFO
    alignCenter(
      createTeamStatsColumns(
        center(teamOne.name, columnMaxWidth),
        teamOneNumber,
        teamOne.rank,
        teamOnePlayers,
        selectedMatch
      )
    ),

    // COLUMN TITLES
    alignCenter([
      '',
      bold('TEAM RANK'),
      bold('PLAYER HLTV RATINGS'),
      '',
      '',
      '',
      '',
      bold(`${selectedMatch.odds[0].provider}`),
      bold(`${selectedMatch.odds[1].provider}`),
      bold('Community Odds'),
    ]),

    // TEAM TWO INFO
    alignCenter(
      createTeamStatsColumns(
        center(teamTwo.name, columnMaxWidth),
        teamTwoNumber,
        teamTwo.rank,
        teamTwoPlayers,
        selectedMatch
      )
    )
  );

  console.log(gamePreviewTable.toString());
};

export default csgoPreview;
