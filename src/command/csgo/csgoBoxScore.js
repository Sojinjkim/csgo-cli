import { basicTable } from '../../utils/table';
import { bold, neonGreen } from '../../utils/log';
import { mapNames } from '../../utils/mapConstants';

const alignCenter = columns =>
  columns.map(content => ({ content, hAlign: 'left', vAlign: 'center' }));

const checkGameHigh = (players, record, recordVal) => {
  const recordArr = players.map(player => Number.parseFloat(player[record]));
  return recordVal >= Math.max(...recordArr) ? neonGreen(recordVal) : recordVal;
};

const createMapBoxScore = (mapResult, mapNumber, selectedMap) => {
  const teamOnePlayers = mapResult.playerStats.team1;
  const teamTwoPlayers = mapResult.playerStats.team2;
  const currMapName = mapResult.map;
  const boxScoreTable = basicTable();
  const teamOneTotals = {
    hsKills: 0,
    flashAssists: 0,
    killDeathsDifference: 0,
  };
  const teamTwoTotals = {
    hsKills: 0,
    flashAssists: 0,
    killDeathsDifference: 0,
  };

  boxScoreTable.push(
    [
      {
        colSpan: 12,
        content: `Map ${mapNumber + 1} - ${mapNames[currMapName]}`,
        hAlign: 'center',
        vAlign: 'center',
      },
    ],
    [
      {
        colSpan: 12,
        content: `${bold(`${mapResult.team1.name}`)} ${
          selectedMap.result
        } ${bold(`${mapResult.team2.name}`)}`,
        hAlign: 'center',
        vAlign: 'center',
      },
    ],
    [
      {
        colSpan: 12,
        content: mapResult.team1.name,
        hAlign: 'left',
        vAlign: 'center',
      },
    ],
    alignCenter([
      bold('PLAYER'),
      bold('Kills'),
      bold('Assists'),
      bold('Deaths'),
      bold('hsKills'),
      bold('Flash Assists'),
      bold('KAST'),
      bold('KD +/-'),
      bold('ADR'),
      bold('Kills PR'),
      bold('Deaths PR'),
      bold('Rating'),
    ])
  );

  teamOnePlayers.forEach(player => {
    const {
      name,
      kills,
      hsKills,
      assists,
      flashAssists,
      deaths,
      KAST,
      killDeathsDifference,
      ADR,
      killsPerRound,
      deathsPerRound,
      rating,
    } = player;

    teamOneTotals.hsKills += hsKills;
    teamOneTotals.flashAssists += flashAssists;
    teamOneTotals.killDeathsDifference += killDeathsDifference;

    boxScoreTable.push(
      alignCenter([
        bold(`${name}`),
        checkGameHigh(teamOnePlayers, 'kills', kills, 35),
        checkGameHigh(teamOnePlayers, 'assists', assists, 10),
        checkGameHigh(teamOnePlayers, 'deaths', deaths, 10),
        checkGameHigh(teamOnePlayers, 'hsKills', hsKills, 15),
        checkGameHigh(teamOnePlayers, 'flashAssists', flashAssists, 10),
        checkGameHigh(teamOnePlayers, 'KAST', KAST, 10),
        checkGameHigh(
          teamOnePlayers,
          'killDeathsDifference',
          killDeathsDifference,
          5
        ),
        checkGameHigh(teamOnePlayers, 'ADR', ADR, 5),
        checkGameHigh(teamOnePlayers, 'killsPerRound', killsPerRound, 5),
        checkGameHigh(teamOnePlayers, 'deathsPerRound', deathsPerRound, 6),
        checkGameHigh(teamOnePlayers, 'rating', rating, 20),
      ])
    );
  });

  // COLLECTIVE TEAM STATS AT THE END OF THE MAP!

  boxScoreTable.push(
    alignCenter([
      'Totals',
      bold(`${mapResult.performanceOverview.team1.kills}`),
      bold(`${mapResult.performanceOverview.team1.assists}`),
      bold(`${mapResult.performanceOverview.team1.deaths}`),
      bold(`${teamOneTotals.hsKills}`),
      bold(`${teamOneTotals.flashAssists}`),
      '-',
      bold(`${teamOneTotals.killDeathsDifference}`),
      '-',
      '-',
      '-',
      '-',
    ])
  );

  boxScoreTable.push(
    [
      {
        colSpan: 12,
        content: '',
        hAlign: 'center',
        vAlign: 'center',
      },
    ],
    [
      {
        colSpan: 12,
        content: mapResult.team2.name,
        hAlign: 'left',
        vAlign: 'center',
      },
    ],
    alignCenter([
      bold('PLAYER'),
      bold('Kills'),
      bold('Assists'),
      bold('Deaths'),
      bold('hsKills'),
      bold('Flash Assists'),
      bold('KAST'),
      bold('KD +/-'),
      bold('ADR'),
      bold('Kills PR'),
      bold('Deaths PR'),
      bold('Rating'),
    ])
  );

  teamTwoPlayers.forEach(player => {
    const {
      name,
      kills,
      hsKills,
      assists,
      flashAssists,
      deaths,
      KAST,
      killDeathsDifference,
      ADR,
      killsPerRound,
      deathsPerRound,
      rating,
    } = player;

    teamTwoTotals.hsKills += hsKills;
    teamTwoTotals.flashAssists += flashAssists;
    teamTwoTotals.killDeathsDifference += killDeathsDifference;

    boxScoreTable.push(
      alignCenter([
        bold(`${name}`),
        checkGameHigh(teamTwoPlayers, 'kills', kills, 35),
        checkGameHigh(teamTwoPlayers, 'assists', assists, 10),
        checkGameHigh(teamTwoPlayers, 'deaths', deaths, 10),
        checkGameHigh(teamTwoPlayers, 'hsKills', hsKills, 15),
        checkGameHigh(teamTwoPlayers, 'flashAssists', flashAssists, 10),
        checkGameHigh(teamTwoPlayers, 'KAST', KAST, 10),
        checkGameHigh(
          teamTwoPlayers,
          'killDeathsDifference',
          killDeathsDifference,
          5
        ),
        checkGameHigh(teamTwoPlayers, 'ADR', ADR, 5),
        checkGameHigh(teamTwoPlayers, 'killsPerRound', killsPerRound, 5),
        checkGameHigh(teamTwoPlayers, 'deathsPerRound', deathsPerRound, 6),
        checkGameHigh(teamTwoPlayers, 'rating', rating, 20),
      ])
    );
  });

  // COLLECTIVE TEAM STATS AT THE END OF THE MAP!

  boxScoreTable.push(
    alignCenter([
      'Totals',
      bold(`${mapResult.performanceOverview.team2.kills}`),
      bold(`${mapResult.performanceOverview.team2.assists}`),
      bold(`${mapResult.performanceOverview.team2.deaths}`),
      bold(`${teamTwoTotals.hsKills}`),
      bold(`${teamTwoTotals.flashAssists}`),
      '-',
      bold(`${teamTwoTotals.killDeathsDifference}`),
      '-',
      '-',
      '-',
      '-',
    ])
  );

  console.log(boxScoreTable.toString());
};

const csgoBoxScore = (maps, mapResults) => {
  for (let index = 0; index < mapResults.length; index += 1) {
    createMapBoxScore(mapResults[index], index, maps[index]);
  }
};

export default csgoBoxScore;
