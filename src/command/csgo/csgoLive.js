import {
  csgoPlayByPlay,
  getTeamRoundScore,
  selectTeamPlayers,
  getMapScoreTableHeader,
  getTeamMapsRoundScore,
  getTeamBoxscore,
  teamNameColor,
} from '../../utils/csgoLiveUtils';

const csgoLive = (
  teamOne,
  teamTwo,
  selectedMatch,
  mapResults,
  scoreboardUpdate,
  playByPlayUpdate,
  csgoLiveScoreboardComponents
) => {
  const { log } = playByPlayUpdate;
  const {
    screen,
    mapScoreTable,
    mapNameText,
    teamOneNameText,
    teamTwoNameText,
    teamOneScoreText,
    teamTwoScoreText,
    playByPlayBox,
    boxscoreTable,
  } = csgoLiveScoreboardComponents;

  const teamOneScore = getTeamRoundScore(teamOne.id, scoreboardUpdate);
  const teamTwoScore = getTeamRoundScore(teamTwo.id, scoreboardUpdate);

  const teamOneScoreboardPlayers = selectTeamPlayers(
    teamOne.id,
    scoreboardUpdate
  );
  const teamTwoScoreboardPlayers = selectTeamPlayers(
    teamTwo.id,
    scoreboardUpdate
  );

  mapScoreTable.setRows([
    getMapScoreTableHeader(selectedMatch),
    getTeamMapsRoundScore(teamOne, mapResults, selectedMatch, teamOneScore),
    getTeamMapsRoundScore(teamTwo, mapResults, selectedMatch, teamTwoScore),
  ]);

  boxscoreTable.setRows([
    ...getTeamBoxscore(teamOne, teamOneScoreboardPlayers),
    ...getTeamBoxscore(teamTwo, teamTwoScoreboardPlayers),
  ]);

  playByPlayBox.setContent(
    csgoPlayByPlay(
      log,
      scoreboardUpdate.terroristTeamName,
      scoreboardUpdate.ctTeamName
    )
  );
  playByPlayBox.focus();

  teamOneScoreText.setContent(teamOneScore);
  teamTwoScoreText.setContent(teamTwoScore);

  teamOneNameText.setContent(
    teamNameColor(teamOne.name, teamOne.id, scoreboardUpdate)
  );
  teamTwoNameText.setContent(
    teamNameColor(teamTwo.name, teamTwo.id, scoreboardUpdate)
  );

  mapNameText.setContent(scoreboardUpdate.mapName);

  screen.render();
};

export default csgoLive;
