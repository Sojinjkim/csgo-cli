import emoji from 'node-emoji';
import { center } from 'wide-align';

import { bold, localTime, neonGreen } from '../../utils/log';
import { mapWinnerColor } from '../../utils/csgoLiveUtils';
import { basicTable } from '../../utils/table';

const vAlignCenter = columns =>
  columns.map(column => {
    if (typeof column === 'string') {
      return { content: column, vAlign: 'center', hAlign: 'center' };
    }

    return { ...column, vAlign: 'center' };
  });

const mapStatsLeaders = (mapStatOverview, stat) =>
  vAlignCenter([
    {
      colSpan: 4,
      content: `${stat}`,
      hAlign: 'right',
    },
    {
      colSpan: 1,
      content: neonGreen(mapStatOverview.value),
      hAlign: 'center',
    },
    {
      colSpan: 4,
      content: bold(`${mapStatOverview.name}`),
      hAlign: 'left',
    },
  ]);

const csgoScoreboard = (selectedMatch, mapResults) => {
  const scoreboardTable = basicTable();

  const date = new Date(selectedMatch.date);

  let teamOnePlayers = [];
  let teamTwoPlayers = [];

  if (selectedMatch.players.team1.length > 0) {
    teamOnePlayers = selectedMatch.players.team1;
  }
  if (selectedMatch.players.team1.length > 0) {
    teamTwoPlayers = selectedMatch.players.team2;
  }
  const numOfMaps = mapResults.length;

  let teamOneMapScore = 0;
  let teamTwoMapScore = 0;
  for (let index = 0; index < mapResults.length; index += 1) {
    mapResults[index].team1.score > mapResults[index].team2.score
      ? (teamOneMapScore += 1)
      : (teamTwoMapScore += 1);
  }

  scoreboardTable.push(
    vAlignCenter([
      {
        colSpan: 9,
        content: bold(`${selectedMatch.event.name}`),
        hAlign: 'center',
      },
    ]),
    vAlignCenter([
      {
        colSpan: 9,
        content: bold(`${selectedMatch.additionalInfo}`),
        hAlign: 'center',
      },
    ]),
    vAlignCenter([
      {
        colSpan: 2,
        content: bold(selectedMatch.team1.name),
        hAlign: 'center',
      },
      {
        colSpan: 5,
        content: bold('Final'),
        hAlign: 'center',
      },
      {
        colSpan: 2,
        content: bold(selectedMatch.team2.name),
        hAlign: 'center',
      },
    ]),
    vAlignCenter([
      '1',
      {
        content: bold(teamOnePlayers[0] ? teamOnePlayers[0].name : 'N/A'),
        hAlign: 'left',
      },
      bold('Team'),
      bold(mapResults[0].map),
      bold(mapResults[1] ? mapResults[1].map : '-'),
      bold(mapResults[2] ? mapResults[2].map : '-'),
      bold(center('Total', 9)),
      '1',
      {
        content: bold(teamTwoPlayers[0] ? teamTwoPlayers[0].name : 'N/A'),
        hAlign: 'left',
      },
    ]),
    vAlignCenter([
      '2',
      {
        content: bold(teamOnePlayers[1] ? teamOnePlayers[1].name : 'N/A'),
        hAlign: 'left',
      },
      `${selectedMatch.team1.name}`,
      bold(
        `${mapWinnerColor(
          mapResults[0].team1.score,
          mapResults[0].team2.score
        )}`
      ),
      numOfMaps > 1
        ? bold(
            `${mapWinnerColor(
              mapResults[1].team1.score,
              mapResults[1].team2.score
            )}`
          )
        : '-',
      numOfMaps > 2
        ? bold(
            `${mapWinnerColor(
              mapResults[2].team1.score,
              mapResults[2].team2.score
            )}`
          )
        : '-',
      bold(mapWinnerColor(teamOneMapScore, teamTwoMapScore)),
      '2',
      {
        content: bold(teamTwoPlayers[1] ? teamTwoPlayers[1].name : 'N/A'),
        hAlign: 'left',
      },
    ]),
    vAlignCenter([
      '3',
      {
        content: bold(teamOnePlayers[2] ? teamOnePlayers[2].name : 'N/A'),
        hAlign: 'left',
      },
      `${selectedMatch.team2.name}`,
      bold(
        `${mapWinnerColor(
          mapResults[0].team2.score,
          mapResults[0].team1.score
        )}`
      ),
      numOfMaps > 1
        ? bold(
            `${mapWinnerColor(
              mapResults[1].team2.score,
              mapResults[1].team1.score
            )}`
          )
        : '-',
      numOfMaps > 2
        ? bold(
            `${mapWinnerColor(
              mapResults[2].team2.score,
              mapResults[2].team1.score
            )}`
          )
        : '-',
      bold(mapWinnerColor(teamTwoMapScore, teamOneMapScore)),
      '3',
      {
        content: bold(teamTwoPlayers[2] ? teamTwoPlayers[2].name : 'N/A'),
        hAlign: 'left',
      },
    ]),
    vAlignCenter([
      '4',
      {
        content: bold(teamOnePlayers[3] ? teamOnePlayers[3].name : 'N/A'),
        hAlign: 'left',
      },
      {
        colSpan: 5,
        content: bold(`${emoji.get('calendar')} ${localTime(date)}`),
        hAlign: 'center',
      },
      '4',
      {
        content: bold(teamTwoPlayers[3] ? teamTwoPlayers[3].name : 'N/A'),
        hAlign: 'left',
      },
    ]),
    vAlignCenter([
      '5',
      {
        content: bold(teamOnePlayers[4] ? teamOnePlayers[4].name : 'N/A'),
        hAlign: 'left',
      },
      {
        colSpan: 5,
        hAlign: 'center',
      },
      '5',
      {
        content: bold(teamTwoPlayers[4] ? teamTwoPlayers[4].name : 'N/A'),
        hAlign: 'left',
      },
    ])
  );
  for (let index = 0; index < numOfMaps; index += 1) {
    scoreboardTable.push(
      vAlignCenter([
        {
          colSpan: 9,
          hAlign: 'center',
        },
      ]),
      vAlignCenter([
        {
          colSpan: 9,
          content: bold(`Map ${index + 1} Leaders`),
          hAlign: 'center',
        },
      ]),
      mapStatsLeaders(mapResults[index].overview.mostKills, 'Kills'),
      mapStatsLeaders(mapResults[index].overview.mostDamage, 'ADR'),
      mapStatsLeaders(mapResults[index].overview.mostAssists, 'Assists'),
      mapStatsLeaders(mapResults[index].overview.mostFirstKills, 'First Kills'),
      mapStatsLeaders(mapResults[index].overview.bestRating, 'Rating')
    );
  }

  console.log(scoreboardTable.toString());
};

export default csgoScoreboard;
