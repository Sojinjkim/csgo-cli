/* eslint-disable no-param-reassign */

import program from 'commander';
import didYouMean from 'didyoumean';
import isAsyncSupported from 'is-async-supported';
import chalk from 'chalk';
import updateNotifier from 'update-notifier';

import { csgo as csgoCommand } from './command';
import { error, bold, neonRed, neonGreen } from './utils/log';

import pkg from '../package.json';

if (!isAsyncSupported()) {
  require('async-to-gen/register');
}

(async () => {
  await updateNotifier({
    pkg,
  }).notify({ defer: false });
})();

program.version(
  `\n${chalk`{bold.hex('#0069b9') CSGO}`} ${neonRed('CLI')} version: ${
    pkg.version
  }\n`,
  '-v, --version'
);

program
  .command('csgo')
  .alias('C')
  .option('-d, --date <date>', 'Watch games at specific date')
  .option('-y, --yesterday', "Watch yesterday's games")
  .option('-t, --today', "Watch today's games")
  .option('-T, --tomorrow', "Watch tomorrow's games")
  .on('--help', () => {
    console.log('');
    console.log('  Watch CSGO live play-by-play, game preview and box score.');
    console.log(
      '  The games displayed will be those of importance (at least 1 Star ratings on HLTV.org) and those recently played, live, or upcoming.'
    );
    console.log('');
    console.log('  Example:');
    console.log(
      `           ${neonGreen(
        'csgo-cli csgo -t'
      )}            => Show game schedule.`
    );
    console.log('');
  })
  .action(option => {
    if (
      !option.date &&
      !option.yesterday &&
      !option.today &&
      !option.tomorrow
    ) {
      option.today = true;
    }

    csgoCommand(option);
  });

program.on('--help', () => {
  console.log('');
  console.log('');
  console.log(
    `  Welcome to ${chalk`{bold.hex('#0069b9') CSGO}`} ${neonRed('cLI')} !`
  );
  console.log('');
  console.log(
    `  Wanna watch CSGO game please enter: ${neonGreen('csgo-cli csgo')}`
  );
  // console.log(
  //   `  Wanna check NBA player information please enter: ${neonGreen(
  //     'nba-go player <name>'
  //   )}`
  // );
  console.log('');
  // console.log(
  //   `  For more detailed information please check the GitHub page: ${neonGreen(
  //     'https://github.com/xxhomey19/nba-go'
  //   )}`
  // );
  // console.log(
  //   `  Or enter ${neonGreen('nba-go game -h')}, ${neonGreen(
  //     'nba-go player -h'
  //   )} to get more helpful information.`
  // );
  console.log('');
});

program.command('*').action(command => {
  error(`Unknown command: ${bold(command)}`);

  const commandNames = program.commands
    .map(c => c._name)
    .filter(name => name !== '*');

  const closeMatch = didYouMean(command, commandNames);

  if (closeMatch) {
    error(`Did you mean ${bold(closeMatch)} ?`);
  }

  process.exit(1);
});

if (process.argv.length === 2) program.help();

program.parse(process.argv);
