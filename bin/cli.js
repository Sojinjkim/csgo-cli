#!/usr/bin/env node

if (process.env.NODE_ENV === 'development') {
  require('@babel/register');

  require('../src/cli');
} else {
  console.log(
    'Only use this in dev mode pl0x. set NODE_ENV=development when running'
  );
}
